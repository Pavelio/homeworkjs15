$(document).ready(function () {
    $('#navbar').on('click', 'a', function (event) {
        event.preventDefault();
        let anchor = $($(this).attr("href")).offset().top;
        $('html, body').animate({ scrollTop: anchor }, 1500);
    });
});

$('#buttonDisplay').on('click', function () {
    $('#section-toprated').slideToggle(500);
});

$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 560) $('#top').fadeIn();
        else $('#top').fadeOut();
    });
    $('#top').click(function () {
        $('body, html').animate({
            scrollTop: 0
        }, 1000);
    });
});